package ch.jmnetwork.launch;

import ch.jmnetwork.vapi.VersionApi;
import ch.jmnetwork.vapi.events.VapiListener;
import ch.jmnetwork.vapi.network.NetworkHelper;
import ch.jmnetwork.vapi.network.NetworkHelper.FileDownloader;
import ch.jmnetwork.vapi.util.PropertiesHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class JLauncher implements VapiListener {

    static String installedVersion;
    private JFrame frmJlauncher;
    static JLabel lblModpack;
    static JLabel lblModpackurl;
    static JLabel lblInstalledversion;
    static JLabel lblNewestversion;
    static VersionApi vapi = new VersionApi("versionApiConfig.xml");
    static PropertiesHandler ph = new PropertiesHandler("JLauncherConfig.xml");
    static NetworkHelper netHelper;
    static JTextField txtMinecraftLoginName;
    static JPasswordField passwordField;
    private static String lastSessionUsername;
    private static String lastSessionPassword;
    JProgressBar progressBar;
    long currentCLength, CLength;
    int progress = 0;

    /**
     * Launch the application.
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    public static void main(String[] args) throws URISyntaxException, IOException {

        doReallyFirst();

        EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    JLauncher window = new JLauncher();
                    doOnStart();
                    window.frmJlauncher.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public JLauncher() {

        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {

        File f = new File("./jlauncher.png");
        vapi.addListener(this);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        frmJlauncher = new JFrame();
        frmJlauncher.setIconImage(Toolkit.getDefaultToolkit().getImage(f.getAbsolutePath()));
        frmJlauncher.getContentPane().setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        frmJlauncher.getContentPane().setLayout(null);

        JSeparator separator = new JSeparator();
        separator.setOrientation(SwingConstants.VERTICAL);
        separator.setBounds(429, 11, 2, 440);
        frmJlauncher.getContentPane().add(separator);

        lblModpack = new JLabel("Modpack");
        lblModpack.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        lblModpack.setBounds(10, 11, 409, 14);
        frmJlauncher.getContentPane().add(lblModpack);

        JButton btnDownload = new JButton("Download / Update");
        btnDownload.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {

                // ======================================//
                // DOWNLOAD / UPDATE SECTION
                // ======================================//
                File f = new File("minecraftDir");

                VersionApi.delete(f);

                vapi.downloadNewVersion(vapi.getNewestVersion(), "minecraftDir");

                // final steps happen in downloadComplete()

            }
        });
        btnDownload.setBounds(10, 428, 160, 23);
        frmJlauncher.getContentPane().add(btnDownload);

        lblModpackurl = new JLabel("Modpackurl");
        lblModpackurl.setForeground(Color.DARK_GRAY);
        lblModpackurl.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        lblModpackurl.setBounds(10, 66, 399, 14);
        frmJlauncher.getContentPane().add(lblModpackurl);

        lblInstalledversion = new JLabel("InstalledVersion");
        lblInstalledversion.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        lblInstalledversion.setBounds(10, 27, 399, 14);
        frmJlauncher.getContentPane().add(lblInstalledversion);

        lblNewestversion = new JLabel("NewestVersion");
        lblNewestversion.setForeground(Color.DARK_GRAY);
        lblNewestversion.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        lblNewestversion.setBounds(10, 52, 399, 14);
        frmJlauncher.getContentPane().add(lblNewestversion);

        JButton btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {

                // ======================================//
                // DELETE SECTION
                // ======================================//

                VersionApi.delete(new File("minecraftDir"));
                ph.setProperty("installed_version", "NOT INSTALLED");
                ph.saveProperties();

                lblInstalledversion.setText("Installed Version: " + ph.getProperty("installed_version", "NOT INSTALLED"));
            }
        });
        btnDelete.setBounds(330, 428, 89, 23);
        frmJlauncher.getContentPane().add(btnDelete);

        txtMinecraftLoginName = new JTextField();
        txtMinecraftLoginName.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        txtMinecraftLoginName.addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {

                ph.setProperty("lastSessionUsername", txtMinecraftLoginName.getText());
                ph.saveProperties();
            }
        });
        txtMinecraftLoginName.setToolTipText("Minecraft Login Name");
        txtMinecraftLoginName.setBounds(441, 276, 233, 20);
        frmJlauncher.getContentPane().add(txtMinecraftLoginName);
        txtMinecraftLoginName.setColumns(10);

        JButton btnPlay = new JButton("Play");
        btnPlay.setBounds(585, 428, 89, 23);
        frmJlauncher.getContentPane().add(btnPlay);

        JButton btnCheck = new JButton("Check");
        btnCheck.setBounds(441, 428, 89, 23);
        frmJlauncher.getContentPane().add(btnCheck);

        passwordField = new JPasswordField();
        passwordField.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        passwordField.addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {

                ph.setProperty("lastSessionPassword", new String(passwordField.getPassword()));
                ph.saveProperties();
            }
        });
        passwordField.setToolTipText("Minecraft Login Password");
        passwordField.setBounds(441, 339, 233, 20);
        frmJlauncher.getContentPane().add(passwordField);

        JLabel lblMinecraftLoginName = new JLabel("Minecraft Login Name");
        lblMinecraftLoginName.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        lblMinecraftLoginName.setBounds(441, 251, 233, 14);
        frmJlauncher.getContentPane().add(lblMinecraftLoginName);

        JLabel lblPassword = new JLabel("Minecraft Login Password");
        lblPassword.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 11));
        lblPassword.setBounds(441, 314, 233, 14);
        frmJlauncher.getContentPane().add(lblPassword);

        progressBar = new JProgressBar();
        progressBar.setBounds(180, 428, 140, 23);

        frmJlauncher.getContentPane().add(progressBar);
        frmJlauncher.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 12));
        frmJlauncher.setTitle("JLauncher");
        frmJlauncher.setBounds(100, 100, 700, 500);
        frmJlauncher.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private static void doReallyFirst() throws URISyntaxException, IOException {
        netHelper = new NetworkHelper(vapi.getVersioningURL());

        // ======================================//
        // DOWNLOAD WINDOW ICON IF NOT THERE
        // ======================================//

        if (!new File("./jlauncher.png").exists()) {
            FileDownloader fd = netHelper.saveURLto("http://www.jmnetwork.ch/public/jlauncher.png", "./jlauncher.png");

            while (!fd.isComplete()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            restartApp();
        }
    }

    private static void doOnStart() throws URISyntaxException, IOException {

        // ======================================//
        // TEST IF WORKING CONFIG IS THERE
        // ======================================//

        if (!vapi.isVersioningURLSet()) {
            vapi.setVersioningURL(JOptionPane.showInputDialog("Enter Modpack URL (Must be VersionAPI compatible)", new String("http://www.jmnetwork.ch/public/jm-modpack/")));
            vapi.reload();
            restartApp();
        }

        // ======================================//
        // INITIALIZING LABELS & CO
        // ======================================//

        installedVersion = ph.getProperty("installed_version", "NOT INSTALLED");
        lastSessionUsername = ph.getProperty("lastSessionUsername");
        lastSessionPassword = ph.getProperty("lastSessionPassword");

        lblModpack.setText("JModpack v1.0");
        lblModpackurl.setText(vapi.getVersioningURL());
        lblInstalledversion.setText("Installed Version: " + installedVersion);
        lblNewestversion.setText("Newest Version: " + vapi.getNewestVersion());
        txtMinecraftLoginName.setText(lastSessionUsername);
        passwordField.setText(lastSessionPassword);

        ph.saveProperties();
    }

    private static void restartApp() throws URISyntaxException, IOException {
        final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        final File thisJAR = new File(JLauncher.class.getProtectionDomain().getCodeSource().getLocation().toURI());

        System.out.println("Restarting: " + javaBin + " -jar " + thisJAR.getPath());

        if (!thisJAR.getName().endsWith(".jar")) return;

        final ArrayList<String> command = new ArrayList<String>();
        command.add(javaBin);
        command.add("-jar");
        command.add(thisJAR.getPath());

        final ProcessBuilder pb = new ProcessBuilder(command);
        pb.start();
        System.exit(0);
    }

    /**
     * This gets activated from VersionApi if a version download completed.
     */
    @Override
    public void downloadComplete() {

        ph.setProperty("installed_version", vapi.getNewestVersion());
        ph.saveProperties();

        lblInstalledversion.setText("Installed Version: " + ph.getProperty("installed_version", "NOT INSTALLED"));

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                progressBar.setValue(0);
                progressBar.setEnabled(false);
            }
        });
        progress = 0;
    }

    @Override
    public void contentLengthAvialable(Long contentLength) {
        CLength = contentLength;

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                progressBar.setEnabled(true);
            }
        });
    }

    @Override
    public void updateDownloadedLength(Long downloadedLength) {
        progress = (int) (((float) downloadedLength / (float) CLength) * 100F);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                progressBar.setValue(progress);
            }
        });
    }
}
